let student = [];
function addStudent(toAdd){
	student.push(toAdd.toLowerCase());
	console.log(toAdd,"was added to the student's list.");
}
function countStudents(){
	console.log("There are a total of "+student.length+" students enrolled.");
}
function printStudents(){
	student.sort();
	student.forEach(function(a){
		console.log(a);
	})
}
function findStudent(key){
	key= key.toLowerCase();
	let filtered = student.filter(x=>x==key);
	if(filtered.length==1){
		console.log(key,"is an enrollee.")
	}else if(filtered.length>1){
		console.log(key,"`s are enrollees.")
	}else{
		console.log(key,"is not an enrollee.")
	}
}
///////////////Stretch Goals//////////
function addSection(){
	console.log(student.map(function(x){
		return x+" - Section A";
	} ));
}
function removeStudent(key){

	key= key[0].toUpperCase()+key.slice(1);
	let index= student.indexOf(key.toLowerCase());
	let removed =  student.splice(index,1);
	console.log(key,"was removed from the student's list");
}

